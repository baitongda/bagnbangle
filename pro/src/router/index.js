import Vue from 'vue'
import Router from 'vue-router'
import Bwmfb from '../views/Bwmfb'
import Bwmxlfb from '@/views/Bwmxlfb'
import Bwsfb from '@/views/Bwsfb'
import Bwsxlfb from '@/views/Bwsxlfb'
import Bwqfb from '@/views/Bwqfb'
import Bwqxlfb from '@/views/Bwqxlfb'
import Bpdfb from '@/views/Bpdfb'
import Bpdxlfb from '@/views/Bpdxlfb'
import Bbmfb from '@/views/Bbmfb'
import Bbmxlfb from '@/views/Bbmxlfb'
import Gmdzfb from '@/views/Gmdzfb'
import Shdzfb from '@/views/Shdzfb'
import Gmsjfb from '@/views/Gmsjfb'
import Yhfb from '@/views/Yhfb'
import Spjgfb from '@/views/Spjgfb'
import Xdfb from '@/views/Xdfb'
import Zyfb from '../views/Zyfb'
// import AMap from 'vue-amap'
import Xxlb from '../views/Xxlb'
import Zcymfb from '../views/Zcymfb'
import Zcsrymfb from '../views/Zcsrymfb'
import Zhmmdlfb from '../views/Zhmmdlfb'
import Dxdlfb from '../views/Dxdlfb'
import Wjmmfb from '../views/Wjmmfb'
import Xgmmfb from '../views/Xgmmfb'
import Xzcsfb from '../views/Xzcsfb'
import Lfb from '../views/Lfb';
import Zyzlfb from '@/views/Zyzlfb'
import Kffb from '@/views/Kffb'
import Ddwtfb from '@/views/Ddwtfb'
import Pswtfb from '@/views/Pswtfb'
import Fywtfb from '@/views/Fywtfb'
import Tsfb from '@/views/Tsfb'
import Fkzxymfb from '@/views/Fkzxymfb'
import Kfzxymfb from '@/views/Kfzxymfb'
import Zhszfb from '@/views/Zhszfb'
import Xgsjhfb from '@/views/Xgsjhfb'
import Szmmfb from '@/views/Szmmfb'
import Dskzhfb from '@/views/Dskzhfb'
import Xgmmfbs from '@/views/Xgmmfbs'
import Xxtzfb from '../views/xxtzfb' 
import Grxxfb from '../views/grxxfb' 
import Yefb from '../views/yefb' 
import Czfb from '../views/czfb'
import Ddglfb from '../views/ddglfb'
import Home from '../views/home'
import All from '../views/all'
import Unpaid from '../views/unpaid'
import Waiting from '../views/waiting'
import Underway from '../views/underway'
import Consignee from '../views/consignee'
import Yqyjfb from '../views/yqyjfb'
import Wddzfb from '../views/wddzfb'
import Glfb from '../views/glfb'
import Jfb from '../views/jfb'
import Company from '../views/company'
import Family from '../views/Family'
import Gdfwjlfb from '../views/gdfwjlfb'
import Wdyhjfb from '../views/wdyhjfb'
Vue.use(Router);
export default new Router({
  routes: [
    {
      path:'/bwmfb',
      component:Bwmfb
    },
    {
      path:'/bwmxlfb',
      component:Bwmxlfb
    },
    {
      path:'/bwsfb',
      component:Bwsfb
    },
    {
      path:'/bwsxlfb',
      component:Bwsxlfb
    },
    {
      path:'/bwqfb',
      component:Bwqfb
    },
    {
      path:'/bwqxlfb',
      component:Bwqxlfb
    },
    {
      path:'/bpdfb',
      component:Bpdfb
    },
    {
      path:'/bpdxlfb',
      component:Bpdxlfb
    },
    {
      path:'/bbmfb',
      component:Bbmfb
    },
    {
      path:'/bbmxlfb',
      component:Bbmxlfb
    },
    {
      path:'/gmdzfb',
      component:Gmdzfb
    },
    {
      path:'/shdzfb',
      component:Shdzfb
    },
    {
      path:'/gmsjfb',
      component:Gmsjfb
    },
    {
      path:'/yhfb',
      component:Yhfb
    },
    {
      path:'/Spjgfb',
      component:Spjgfb
    },
    {
      path:'/Xdfb',
      component:Xdfb
    },
    {
      path:'/zyfb',
      component:Zyfb,
    },
    {
      path:'/zcsrymfb',
      component:Zcsrymfb
      },
    {
      path:'/zhmmdlfb',
      component:Zhmmdlfb
        },  
    {
      path:'/zcymfb',
      component:Zcymfb,
      },
    {
      path:'/dxdlfb',
      component:Dxdlfb
     },
    {
      path:'/wjmmfb',
      component:Wjmmfb
    },
    {
      path:'/xgmmfb',
      component:Xgmmfb
    },
    {
      path:'/xzcsfb',
      component:Xzcsfb
    },
    {
      path:'/xxlb',
      component:Xxlb
    },
    {
      path:'/zyzlfb',
      component:Zyzlfb
    },
    {
      path:'/bwmfb',
      component:Bwmfb
    },
    {
      path:'/bwsfb',
      component:Bwsfb
    },
    {
      path:'/bwqfb',
      component:Bwqfb
    },
    {
      path:'/bbmfb',
      component:Bbmfb
    },
    {
      path:'/zyzlfb',
      component:Zyzlfb
    },
    {
      path:'/kffb',
      component:Kffb
    },
    {
      path:'/ddwtfb',
      component:Ddwtfb
  },
  {
    path:'/pswtfb',
    component:Pswtfb
  },
  {
    path:'/fywtfb',
    component:Fywtfb
  },
  {
    path:'/tsfb',
    component:Tsfb
  },
  {
    path:'/fkzxymfb',
    component:Fkzxymfb
  },
  {
    path:'/kfzxymfb',
    component:Kfzxymfb
  },
  {
    path:'/zhszfb',
    component:Zhszfb
  },
    {
      path: '/xgsjhfb',
      component: Xgsjhfb
    },
    {
      path:'/szmmfb',
      component:Szmmfb
    },
    {
      path:'/dskzhfb',
      component:Dskzhfb
    },
    {
      path:'/xgmmfbs',
      component:Xgmmfbs
    },
    {
      path:'/lfb',
      component:Lfb
      },
    {
      path:'/xxlb',
      component:Xxlb
    },
    {
      path:'/xxtzfb',
      component:Xxtzfb
    },
    {
      path:'/grxxfb',
      component:Grxxfb
    },
    {
      path:'/yefb',
      component:Yefb
    },
    {
      path:'/czfb',
      component:Czfb
    },
    {
      path:'/ddglfb',
      component:Ddglfb,
      children:[
        {
          path:'/',
          component:Home
        },
        {
          path:'/all',
          component:All
        },
        {
          path:'/unpaid',
          component:Unpaid
        },
        {
          path:'/waiting',
          component:Waiting
        },
        {
          path:'/underway',
          component:Underway
        },
        {
          path:'/consignee',
          component:Consignee
        },
      ]
    },
    {
      path:'/yqyjfb',
      component:Yqyjfb
    },
    {
      path:'/wddzfb',
      component:Wddzfb,
      children:[
        
      ]
    },
    {
      path:'/glfb',
      component:Glfb
    },
    {
      path:'/jfb',
      component:Jfb,
      children:[
        {
          path:'/company',
          component:Company
        },
        {
          path:'/family',
          component:Family
        },
      ]
    },
    {
      path:'/gdfwjlfb',
      component:Gdfwjlfb,
      children:[
        
      ]
    },
    {
      path:'/wdyhjfb',
      component:Wdyhjfb
    },
    {
      path: '/',
      redirect:'/lfb'
      },
  ]
})

