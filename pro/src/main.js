// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueAMap from 'vue-amap';
import router from './router'
import Vant from 'vant';
import 'vant/lib/index.css'
import 'lib-flexible';
import './assets/iconfont/iconfont.css';
import 'lib-flexible/flexible'


// import store from './store'
// import './data/userinfo.js'
// import './data/prolist.js'


Vue.config.productionTip = false
Vue.use(Vant);
Vue.use(VueAMap);
VueAMap.initAMapApiLoader({
  key: '66208620ccabea623ba4b788aeaaa2ce',
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],
  // 默认高德 sdk 版本为 1.4.4
  v: '1.4.4'
});
/* eslint-disable no-new */
 

new Vue({
  el: '#app',
  router,
  // store,
  components: { App },
  template: '<App/>'
})
